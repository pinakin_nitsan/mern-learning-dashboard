import React from "react";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import { Formik } from "formik";
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { signup } from "../../actions/userActions";

const signUpSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Email is required"),
  password: Yup.string()
    .min(8, "Password is too short!")
    .max(16, "Password is too long!")
    .required("Password is required"),
});

const Signup = (props) => {
  return (
    <section className="auth-section signup-section">
      <div className="container">
        <h2>Sign up</h2>
        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={signUpSchema}
          onSubmit={(values) => {
            props.signup(values.email, values.password);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email && (
                  <div className="error-text">{errors.email}</div>
                )}
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <div className="error-text">{errors.password}</div>
                )}
              </Form.Group>
              <div className="action-box">
                <div className="redirect-to-auth">
                  <Link to="/login">Log in</Link>
                </div>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, { signup })(Signup);
