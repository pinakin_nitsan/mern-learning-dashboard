import React from "react";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import { Formik } from "formik";
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { login } from "../../actions/userActions";

const logInSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Email is required"),
  password: Yup.string().required("Password is required"),
});

const Login = (props) => {
  return (
    <section className="auth-section login-section">
      <div className="container">
        <h2>Login</h2>
        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={logInSchema}
          onSubmit={(values) => {
            props.login(values.email, values.password);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email && (
                  <div className="error-text">{errors.email}</div>
                )}
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <div className="error-text">{errors.password}</div>
                )}
              </Form.Group>
              <div className="action-box">
                <div className="redirect-to-auth">
                  <Link to="/signup">Create an account</Link>
                </div>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </div>
              {props.user.error && (
                <div className="error-text">{props.user.error}</div>
              )}
            </Form>
          )}
        </Formik>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, { login })(Login);
