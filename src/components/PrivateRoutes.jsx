import React from 'react';
import { Redirect } from 'react-router-dom';

// export const PrivateRoute = ({ component: Component, ...rest }) => (
//     <Route {...rest} render={props => (
//         localStorage.getItem('token')
//             ? <Component {...props} />
//             : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
//     )} />
// )

export const PrivateRoute = (props) => (
    <>
    {
        localStorage.getItem('token')
            ? props.children
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    }
    </>
)