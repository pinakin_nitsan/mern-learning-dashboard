import React, { useEffect, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import CategoryForm from "./CategoryForm";
import Api from "../../utils/api";

function CategoryEdit({ show, handleClose, categoryData }) {
  const api = new Api();
  const [category, setCategory] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!categoryData || !categoryData.slug) return;
    (async () => {
      const resCategory = await api.getCategory(categoryData.slug);
      setLoading(false);
      setCategory(resCategory.data.data.category);
    })();
  }, [categoryData]);

  const handleUpdateCategory = async (values) => {
    const { name, title, thumbnail, popular, season, city } = values;
    await api.updateCategory(values._id, {
      name,
      title,
      thumbnail,
      popular,
      season,
      city,
    });
  };

  const renderBody = () => {
    if (loading) {
      return <div>Loading...</div>;
    }

    if (!category) {
      return <div>No data found</div>;
    }

    return (
      <CategoryForm
        action={handleUpdateCategory}
        values={category}
        handleClose={handleClose}
      />
    );
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>{renderBody()}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CategoryEdit;
