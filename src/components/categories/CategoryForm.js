import React, { useState } from "react";
import * as Yup from "yup";
import { connect } from "react-redux";
import Resizer from "react-image-file-resizer";
import { Formik, Field } from "formik";
import { Form, Button, Spinner } from "react-bootstrap";
import { getCategories } from "../../actions/categoryActions";
import Api from "../../utils/api";

const categorySchema = Yup.object().shape({
  name: Yup.string().required("Name is required"),
  title: Yup.string().required("Title is required"),
});

const CategoryForm = ({ values, handleClose, getCategories, action }) => {
  const api = new Api();
  const [thumbnail, setThumbnail] = useState(
    values.thumbnail ? values.thumbnail : ""
  );
  const [loading, setLoading] = useState(false);
  const initialValues = {
    name: "",
    title: "",
    popular: false,
    season: false,
    city: false,
  };

  return (
    <Formik
      initialValues={values ? values : initialValues}
      validationSchema={categorySchema}
      onSubmit={async (values) => {
        console.log({ ...values, thumbnail }, "values");
        await action({ ...values, thumbnail });
        getCategories("");
        handleClose();
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="formBasicName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.name}
            />
            {errors.name && touched.name && (
              <div className="error-text">{errors.name}</div>
            )}
          </Form.Group>

          <Form.Group controlId="formBasicTitle">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              name="title"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.title}
            />
            {errors.password && touched.password && (
              <div className="error-text">{errors.password}</div>
            )}
          </Form.Group>
          <Form.Group controlId="formBasicThumbnail">
            <Form.Label>Thumbnail</Form.Label>
            <Form.Control
              type="file"
              name="thumbnail"
              onChange={async (e) => {
                Resizer.imageFileResizer(
                  e.target.files[0],
                  720,
                  720,
                  "JPEG",
                  100,
                  0,
                  (uri) => {
                    setLoading(true);
                    setThumbnail("");
                    api
                      .uploadImage(uri)
                      .then((res) => {
                        setLoading(false);
                        setThumbnail(res.data.data.url);
                      })
                      .catch((e) => {
                        setLoading(false);
                        console.log(e);
                      });
                  },
                  "base64"
                );
              }}
              onBlur={handleBlur}
            />
            {loading && (
              <div className="spinning-wrapper">
                <Spinner animation="border" role="status"></Spinner>
              </div>
            )}
            {thumbnail && (
              <div
                style={{
                  backgroundImage: `url(${thumbnail})`,
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  maxWidth: "100%",
                  paddingBottom: "75%",
                  marginTop: "15px",
                }}
              ></div>
            )}
            {errors.thumbnail && touched.thumbnail && (
              <div className="error-text">{errors.thumbnail}</div>
            )}
          </Form.Group>
          <Form.Group controlId="formBasicPopular">
            <label>
              <input
                type="checkbox"
                name="popular"
                onChange={handleChange}
                checked={values.popular}
              />
              Popular
            </label>
          </Form.Group>
          <Form.Group controlId="formBasicSeason">
            <label>
              <input
                type="checkbox"
                name="season"
                onChange={handleChange}
                checked={values.season}
              />
              Season
            </label>
          </Form.Group>
          <Form.Group controlId="formBasicCity">
            <label>
              <input
                type="checkbox"
                name="city"
                onChange={handleChange}
                checked={values.city}
              />
              City
            </label>
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default connect(null, { getCategories })(CategoryForm);
