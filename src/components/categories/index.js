import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import { Modal, Button } from "react-bootstrap";
import Select from "react-select";
import { getCategories } from "../../actions/categoryActions";
import history from "../../history";
import CategoryEdit from "./CategoryEdit";
import Api from "../../utils/api";
import CategoryCreate from "./CategoryCreate";

const Categories = ({ categories, getCategories }) => {
  const api = new Api();
  const [editShow, setEditShow] = useState(false);
  const [createShow, setCreateShow] = useState(false);
  const [deleteCategory, setDeleteCategory] = useState(false);
  const [disableDelete, setDisableDelete] = useState(false);
  const [currentCategory, setCurrentCategory] = useState(null);
  const [category, setCategory] = useState({ label: "All", value: "" });
  const [modifiedCategories, setModifiedCategories] = useState(
    categories.data ? categories : []
  );

  const categoryTypes = [
    { label: "All", value: "" },
    { label: "Popular", value: "popular" },
    { label: "City", value: "city" },
    { label: "Seasons", value: "season" },
  ];

  useEffect(() => {
    getCategories(category.value);
  }, []);

  useEffect(() => {
    if (categories.isLoading) return;
    const changedCategories = categories.data.map((category, id) => {
      return { ...category, id: id + 1 };
    });
    setModifiedCategories(changedCategories);
  }, [categories]);

  const categoryTypeChange = (categoryType) => {
    setCategory(categoryType);
  };

  const handleEditClose = () => setEditShow(false);
  const handleEditShow = () => setEditShow(true);
  const handleCreateClose = () => setCreateShow(false);
  const handleCreateShow = () => setCreateShow(true);
  const handleDeleteCategoryClose = () => setDeleteCategory(false);
  const handleDeleteCategoryShow = () => setDeleteCategory(true);

  const renderTable = () => {
    if (categories.isLoading) {
      return <div>Loading...</div>;
    } else if (!modifiedCategories.length) {
      return <div>No data found!</div>;
    } else {
      return (
        <MaterialTable
          title="List of categories"
          columns={[
            { title: "ID", field: "id" },
            {
              title: "Name",
              field: "name",
              cellStyle: {
                textTransform: "capitalize",
              },
            },
            { title: "Title", field: "title" },
          ]}
          data={modifiedCategories}
          actions={[
            {
              icon: "visibility",
              tooltip: "View Category",
              onClick: (event, rowData) =>
                history.push(`/categories${rowData.urlSlug}`),
            },
            {
              icon: "edit",
              tooltip: "Edit Category",
              onClick: (event, rowData) => {
                handleEditShow();
                setCurrentCategory(rowData);
              },
            },
            {
              icon: "delete",
              tooltip: "Delete Category",
              onClick: (event, rowData) => {
                handleDeleteCategoryShow();
                setCurrentCategory(rowData);
              },
            },
          ]}
          options={{
            actionsColumnIndex: -1,
            filtering: false,
            pageSize: 8,
            pageSizeOptions: [8, 16, 24],
          }}
        />
      );
    }
  };

  return (
    <div className="categories-block">
      <h1>Categories</h1>
      <div className="common-panel">
        <div className="actions-wrapper">
          <div className="results-box">{modifiedCategories.length} results</div>
          <div className="choose-category-type-wrapper">
            <Select
              defaultValue={{
                label: category.label,
                value: category.value,
              }}
              placeholder="Select a category type"
              className="react-select-container"
              classNamePrefix="react-select"
              options={categoryTypes}
              // onChange={({ value }) => getCategories(value)}
              onChange={(selectedOption) => {
                categoryTypeChange(selectedOption);
                getCategories(selectedOption.value);
              }}
            />
          </div>
        </div>
        {renderTable()}

        <div className="create-action-wrapper">
          <Button variant="success" onClick={handleCreateShow}>
            Create a category
          </Button>
        </div>

        {createShow && (
          <CategoryCreate
            show={createShow}
            handleClose={handleCreateClose}
            handleShow={handleCreateShow}
          />
        )}

        {editShow && (
          <CategoryEdit
            show={editShow}
            handleClose={handleEditClose}
            handleShow={handleEditShow}
            categoryData={currentCategory}
          />
        )}

        {currentCategory && deleteCategory && (
          <Modal show={deleteCategory} onHide={handleDeleteCategoryClose}>
            <Modal.Header closeButton>
              <Modal.Title>Are you sure you want to delete ?</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              You will not be able to retrieve lost data of{" "}
              {currentCategory && currentCategory.name}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleDeleteCategoryClose}>
                Close
              </Button>
              <Button
                variant="danger"
                disable={disableDelete}
                onClick={async () => {
                  setDisableDelete(true);
                  await api.deleteCategory(currentCategory._id);
                  setDisableDelete(false);
                  handleDeleteCategoryClose();
                  getCategories(category.value);
                }}
              >
                Delete
              </Button>
            </Modal.Footer>
          </Modal>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  categories: state.categories,
});

export default connect(mapStateToProps, { getCategories })(Categories);
