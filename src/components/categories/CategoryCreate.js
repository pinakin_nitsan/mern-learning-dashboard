import React from "react";
import { Modal, Button } from "react-bootstrap";
import CategoryForm from "./CategoryForm";
import Api from "../../utils/api";

function CategoryCreate({ show, handleClose }) {
  const api = new Api();

  const handleCreateCategory = async (values) => {
    const { name, title, thumbnail, popular, season, city } = values;
    await api.createCategory(values._id, {
      name,
      title,
      thumbnail,
      popular,
      season,
      city,
    });
  };

  const renderBody = () => {
    return (
      <CategoryForm action={handleCreateCategory} handleClose={handleClose} />
    );
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Create Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>{renderBody()}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CategoryCreate;
