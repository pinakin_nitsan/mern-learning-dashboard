import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Api from "../../utils/api";

const Category = () => {
  const api = new Api();
  let { name } = useParams();
  const [category, setCategory] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      const resCategory = await api.getCategory(name);
      setLoading(false);
      setCategory(resCategory.data.data.category);
    })();
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!category) {
    return <div>No data found</div>;
  }

  return (
    <div className="single-category-block">
      <h1>{category.name}</h1>
    </div>
  );
};

export default Category;
