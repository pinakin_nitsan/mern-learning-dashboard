import React from "react";
import { NavLink } from "react-router-dom";
import { Button } from "react-bootstrap";

export default function Header(props) {
  return (
    <header className="site-header open">
      <nav className="site-nav">
        <div className="inner-nav">
          <div className="logo-in">
            <NavLink activeClassName="active" to="/">
              Admin
            </NavLink>
          </div>
          <ul className="menu-list">
            <li>
              <NavLink activeClassName="active" exact to="/">
                <button>
                  <span className="text">Categories</span>
                </button>
              </NavLink>
            </li>
            <li>
              <NavLink activeClassName="active" exact to="/places">
                <button>
                  <span className="text">Places</span>
                </button>
              </NavLink>
            </li>
          </ul>
        </div>
        <div className="log-out">
          <Button
            type="button"
            variant="primary"
            onClick={(e) => {
              e.preventDefault();
              props.logout();
            }}
          >
            Log out
          </Button>
        </div>
      </nav>
    </header>
  );
}
