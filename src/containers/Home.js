import React from "react";
import { connect } from "react-redux";
import { logout } from "../actions/userActions";
import Header from "../components/header";

const HomePage = ({ logout, children }) => {
  return (
    <section className="site-wrapper">
      <Header logout={logout} />
      <main className="site-content">
        <div className="top-header">
          <div className="toggle-icon">
            <ul>
              <li></li>
              <li></li>
              <li></li>
            </ul>
          </div>
        </div>
        <div className="content-wrapper">{children}</div>
      </main>
    </section>
  );
};

export default connect(null, { logout })(HomePage);
