import { Router, Route } from "react-router-dom";
import { PrivateRoute } from "./components/PrivateRoutes";
import history from "./history";
import Login from "./components/auth/Login.js";
import Signup from "./components/auth/Signup.js";
import HomePage from "./containers/Home.js";
import Categories from "./components/categories";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Category from "./components/categories/Category";

const App = () => {
  return (
    <div className="App">
      <Router history={history}>
        <>
          <PrivateRoute>
            <Route exact path="/">
              <HomePage>
                <Categories />
              </HomePage>
            </Route>
            <Route exact path="/places">
              <HomePage>Places</HomePage>
            </Route>
            <Route path="/categories/:name">
              <HomePage>
                <Category />
              </HomePage>
            </Route>
          </PrivateRoute>
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
        </>
      </Router>
    </div>
  );
};

export default App;
