import { userConstants } from "../actions/types";

let token = localStorage.getItem("token");
const initialState = token ? { loggedIn: true, token, error: "" } : {};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case userConstants.SIGNUP_REQUEST:
      return {
        loggingIn: true,
        token: action.token,
        error: "",
      };
    case userConstants.SIGNUP_SUCCESS:
      return {
        loggedIn: true,
        token: action.token,
        error: "",
      };
    case userConstants.SIGNUP_FAILURE:
      return {
        error: action.error,
      };
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        token: action.token,
        error: "",
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        token: action.token,
        error: "",
      };
    case userConstants.LOGIN_FAILURE:
      return {
        error: action.error,
      };
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
};

export default userReducer;
