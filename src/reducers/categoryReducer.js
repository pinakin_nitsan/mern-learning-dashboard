import { categoryConstants } from "../actions/types";

const initialState = {
  isLoading: true,
  data: [],
  error: "",
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case categoryConstants.GET_CATEGORIES_REQUEST:
      return {
        isLoading: true,
        data: [],
        error: "",
      };
    case categoryConstants.GET_CATEGORIES_SUCCESS:
      return {
        isLoading: false,
        data: action.categories,
        error: "",
      };
    case categoryConstants.GET_CATEGORIES_FAILURE:
      return {
        isLoading: false,
        data: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default userReducer;
