import { userConstants } from "./types";
import history from "../history";
import Api from "../utils/api";

const api = new Api();

export const signup = (email, password) => {
  const request = (token) => {
    return { type: userConstants.LOGIN_REQUEST, token };
  };
  const success = (token) => {
    return { type: userConstants.LOGIN_SUCCESS, token };
  };
  const failure = (error) => {
    return { type: userConstants.LOGIN_FAILURE, error };
  };

  return async (dispatch) => {
    dispatch(request({ email }));

    try {
      const user = await api.signup(email, password);
      dispatch(success(user.data.data.token));
      localStorage.setItem("token", user.data.data.token);
      history.push("/");
    } catch (e) {
      console.log(e, "e");
      dispatch(failure("Email is already in use"));
    }
  };
};

export const login = (email, password) => {
  const request = (token) => {
    return { type: userConstants.LOGIN_REQUEST, token };
  };
  const success = (token) => {
    return { type: userConstants.LOGIN_SUCCESS, token };
  };
  const failure = (error) => {
    return { type: userConstants.LOGIN_FAILURE, error };
  };

  return async (dispatch) => {
    dispatch(request({ email }));

    // try {
    //   const user = await api.login(email, password);
    //   dispatch(success(user.data.data.token));
    //   localStorage.setItem("token", user.data.data.token);
    //   history.push("/");
    // } catch (e) {
    //   console.log(e, "e");
    //   dispatch(failure("Email or Password is invalid"));
    // }
    api
      .login(email, password)
      .then((user) => {
        dispatch(success(user.data.data.token));
        localStorage.setItem("token", user.data.data.token);
        history.push("/");
      })
      .catch((err) => {
        console.log(err, "err");
        dispatch(failure("Email or Password is invalid"));
      });
  };
};

export const logout = () => {
  return () => {
    api
      .logout()
      .then((user) => {
        localStorage.removeItem("token");
        history.push("/login");
      })
      .catch((error) => {
        console.log("in logout error");
      });
  };
};
