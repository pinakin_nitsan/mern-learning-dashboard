import { categoryConstants } from "./types";
import Api from "../utils/api";

const api = new Api();

export const getCategories = (type) => {
  const request = () => {
    return { type: categoryConstants.GET_CATEGORIES_REQUEST };
  };
  const success = (categories) => {
    return { type: categoryConstants.GET_CATEGORIES_SUCCESS, categories };
  };
  const failure = (error) => {
    return { type: categoryConstants.GET_CATEGORIES_FAILURE, error };
  };

  return async (dispatch) => {
    dispatch(request());

    try {
      const categories = await api.getCategories(type);
      dispatch(success(categories.data.data.categories));
    } catch (e) {
      console.log(e, "e");
      dispatch(failure(e.message));
    }
  };
};

export const getCategory = (type) => {
  const request = () => {
    return { type: categoryConstants.GET_CATEGORY_REQUEST };
  };
  const success = (categories) => {
    return { type: categoryConstants.GET_CATEGORY_SUCCESS, categories };
  };
  const failure = (error) => {
    return { type: categoryConstants.GET_CATEGORY_FAILURE, error };
  };

  return async (dispatch) => {
    dispatch(request());

    try {
      const category = await api.getCategories(type);
      dispatch(success(category.data.data.categories));
    } catch (e) {
      console.log(e, "e");
      dispatch(failure(e.message));
    }
  };
};
