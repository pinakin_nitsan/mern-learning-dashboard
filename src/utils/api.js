import axios from "axios";

export default class Api {
  constructor() {
    this.api_token = null;
    this.client = null;
    this.api_url = "http://localhost:3300/api";
  }

  init = () => {
    this.api_token = localStorage.getItem("token");

    let headers = {
      Accept: "application/json",
    };

    if (this.api_token) {
      headers.Authorization = `Bearer ${this.api_token}`;
    }

    this.client = axios.create({
      baseURL: this.api_url,
      timeout: 31000,
      headers: headers,
    });

    return this.client;
  };

  signup = (email, password) => {
    return this.init().post("/users", {
      email,
      password,
    });
  };

  login = (email, password) => {
    return this.init().post("/users/login", {
      email,
      password,
    });
  };

  logout = () => {
    return this.init().post("/users/logout");
  };

  getCategories = (type = "") => {
    return this.init().get("/categories", {
      params: {
        type,
      },
    });
  };

  getCategory = (categoryName = "") => {
    return this.init().get(`/categories/${categoryName}`);
  };

  createCategory = (id = "", data) => {
    return this.init().post(`/categories/${id}`, { ...data });
  };

  updateCategory = (id = "", data) => {
    return this.init().patch(`/categories/${id}`, { ...data });
  };

  deleteCategory = (id = "") => {
    return this.init().delete(`/categories/${id}`);
  };

  uploadImage = (uri) => {
    return this.init().post(`/uploadimages`, {
      image: uri,
    });
  };
}
